var config = require('../../nightwatch.conf.js');

module.exports = {
    'kooliasjad': function(browser) {
        browser
            .resizeWindow(2000, 1000)
            .url('https://www.tptlive.ee/')
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'tptlive.png')
            .click("#menu-item-1313")
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'tunniplaan.png')
            .useXpath()
            .click("//a[text()='TA-17E']")
            .useCss()
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'mtunniplaan.png')
            .pause(2000)
            .end();
    }
};

